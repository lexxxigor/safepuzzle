#include "safepuzzle.h"
#include "ui_safepuzzle.h"

SafePuzzle::SafePuzzle(QWidget *parent) :
    QMainWindow(parent)
{
    currentState = GameMenu;
    colCount = rowCount = 4;

    configurateUi(currentState);
    setWindowIcon(QIcon(":/icon"));

    QFile fileStyle(":/style");
    if(fileStyle.open(QIODevice::ReadOnly)){
        setStyleSheet(fileStyle.readAll());
        fileStyle.close();
    }

    winList << "лежат ключи и документы от новенького\nPorsche Panamera, бегом за руль и в путь!";
    winList << "лежат путевки в путешествие по жарким странам.\n\"На взлёт и вперед!\"";
    winList << "лежит кругленькая сумма денег,\nможешь потратить её на что угодно!";
    winList << "лежит золотая безлимитная карта, теперь ты властелин мира!\nМУХАХА!";
}

SafePuzzle::~SafePuzzle()
{
    delete ui;
}

void SafePuzzle::configurateUi(GameState state){
    if(ui) delete ui;

    ui = new Ui::SafePuzzle;
    ui->setupUi(this);

    currentState = state;
    globalAnimation = false;
    undo = false;

    mediaObject = new Phonon::MediaObject(this);
    Phonon::AudioOutput *audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
    Phonon::createPath(mediaObject, audioOutput);
    mediaObject->setCurrentSource(click);

    setFixedSize(650, 900);

    switch(state){
        case GameStatistic: {
            setWindowTitle("Статистика игр");
            QGridLayout *centralLayout = new QGridLayout(ui->centralWidget);
            QFrame *frame = new QFrame;
            QVBoxLayout *vLayout = new QVBoxLayout(frame);
            QLabel *headerLabel = new QLabel("Статистика игр");

            centralLayout->setMargin(0);
            centralLayout->addWidget(frame);
            frame->setObjectName("statisticFrame");

            headerLabel->setObjectName("headerLabel");
            vLayout->addWidget(headerLabel, 1, Qt::AlignCenter);

            vLayout->addStretch(7);
            QLabel *info = new QLabel("Тут будет статистика");
            info->setObjectName("infoLabel");
            vLayout->addWidget(info, 1, Qt::AlignCenter);
            vLayout->addStretch(4);

            vLayout->addStretch(7);
        } break;

        case GameSettings: {
            setWindowTitle("Настройки");

            QGridLayout *centralLayout = new QGridLayout(ui->centralWidget);
            QFrame *frame = new QFrame;
            QVBoxLayout *vLayout = new QVBoxLayout(frame);
            QLabel *headerLabel = new QLabel("Статистика игр");

            centralLayout->setMargin(0);
            centralLayout->addWidget(frame);
            frame->setObjectName("settingsFrame");

            headerLabel->setObjectName("headerLabel");
            vLayout->addWidget(headerLabel, 1, Qt::AlignCenter);

            vLayout->addStretch(7);
            QLabel *info = new QLabel("Тут будут настройки");
            info->setObjectName("infoLabel");
            vLayout->addWidget(info, 1, Qt::AlignCenter);
            vLayout->addStretch(4);

            vLayout->addStretch(7);
        } break;

        default:
        case GameMenu: {
            currentState = GameMenu;
            setWindowTitle("Меню");

            QGridLayout *centralLayout = new QGridLayout(ui->centralWidget);
            QFrame *frame = new QFrame;
            QVBoxLayout *vLayout = new QVBoxLayout(frame);

            centralLayout->setMargin(0);
            centralLayout->addWidget(frame);
            frame->setObjectName("menuFrame");

            vLayout->addWidget(new QLabel("Что же внутри сейфа?"), 1, Qt::AlignCenter);
            vLayout->addStretch(4);

            QPushButton *btn = NULL;
            btn = new QPushButton("Новая игра", this);
            btn->setObjectName("newGameButton");
            btn->setProperty("state", Game);
            connect(btn, SIGNAL(clicked()), this, SLOT(onMenuButtonClicked()));
            vLayout->addWidget(btn, 1, Qt::AlignCenter);

            btn = new QPushButton("Статистика игр", this);
            btn->setObjectName("statisticButton");
            btn->setProperty("state", GameStatistic);
            connect(btn, SIGNAL(clicked()), this, SLOT(onMenuButtonClicked()));
            vLayout->addWidget(btn, 1, Qt::AlignCenter);

            btn = new QPushButton("Настройки", this);
            btn->setObjectName("settingsButton");
            btn->setProperty("state", GameSettings);
            connect(btn, SIGNAL(clicked()), this, SLOT(onMenuButtonClicked()));
            vLayout->addWidget(btn, 1, Qt::AlignCenter);

            btn = new QPushButton("Выход", this);
            btn->setObjectName("exitButton");
            connect(btn, SIGNAL(clicked()), this, SLOT(close()));
            vLayout->addWidget(btn, 1, Qt::AlignCenter);

            vLayout->addStretch(7);

        } break;

        case Game: {
            setFixedSize(900, 600);
            setWindowTitle("Вскрытие покажет");

            QGridLayout *gameLayout = new QGridLayout(ui->centralWidget);
            QFrame *mainFrame = new QFrame;
            QFrame *safeFrame = new QFrame(mainFrame);
            QGridLayout *gLayout = new QGridLayout(safeFrame);
            QLabel *gameRule = new QLabel("Чтобы открыть сейф, необходимо привести все рычажки "
                                          "в горизонтальное положение.", mainFrame);
            QPushButton *undoButton = new QPushButton(QIcon(":/undo"), "Отменить шаг", mainFrame);
            undoButton->setObjectName("undoButton");
            undoButton->setDisabled(true);
            connect(undoButton, SIGNAL(clicked()), this, SLOT(onUndoButtonClicked()));

            gameLayout->setMargin(0);
            gameLayout->addWidget(mainFrame);
            mainFrame->setObjectName("gameFrame");
            gameRule->setObjectName("gameRule");
            safeFrame->setObjectName("safeFrame");
            safeFrame->setFixedSize(450, 300);
            safeFrame->move(225, 180);
            gameRule->move(20, 20);
            undoButton->move(390, 120);

            gLayout->setAlignment(Qt::AlignCenter);
            gLayout->addItem(new QSpacerItem(260, 20), 0, 0);

//            QRectF gameRect = QRectF(0,0,240,300);
            QRectF gameRect = QRectF(0,0,250,250);

            for(int row=0; row<rowCount; ++row){
                for(int col=0; col<colCount; ++col){
                    Handle *h = new Handle(gameRect.width()/colCount,
                                           gameRect.height()/rowCount, mediaObject);
                    connect(h, SIGNAL(handleRotated(Handle*)),
                            this, SLOT(onHandleRotated(Handle*)));
                    connect(h, SIGNAL(handleStartRotation(Handle*)),
                            this, SLOT(onHandleStartRotation(Handle*)));
                    connect(this, SIGNAL(globalAnimationChanged(bool)),
                            h, SLOT(onGlobalAnimationChanged(bool)));
                    handleMap.insert(row*colCount+col, h);
                    gLayout->addWidget(h, row+1, col+1);
                }
            }

            gameStartTime = QTime::currentTime();
        } break;
    }

    window()->setGeometry(QStyle::alignedRect(
                              Qt::LeftToRight,
                              Qt::AlignCenter,
                              window()->size(),
                              qApp->desktop()->availableGeometry()));
}

void SafePuzzle::onMenuButtonClicked(){
    configurateUi((GameState)sender()->property("state").toInt());
}

void SafePuzzle::onUndoButtonClicked(){
    if(!globalAnimation){
        if(!savedSteps.isEmpty()){
            Handle *handle = savedSteps.takeLast();
            undo = true;
            handle->animate();
        }
        if(savedSteps.isEmpty()){
            QPushButton *undoButton = findChild<QPushButton*>("undoButton");
            if(undoButton) undoButton->setDisabled(true);
        }
    }
}

void SafePuzzle::onHandleRotated(Handle *h){
    if(globalAnimation && animatedHandles.contains(h)){
        animatedHandles.removeAll(h);
        if(!animatedHandles.isEmpty()) return;
    }
    else if(globalAnimation) return;

    static int r, rt, rb, c, cl, cr;

    if(!globalAnimation){
        globalAnimation = true;
        globalAnimationChanged(globalAnimation);
        int key = handleMap.key(h);
        r = rt = rb = key/rowCount;
        c = cl = cr = key%colCount;
    }

    if(globalAnimation){
        --rt; --cl; ++cr; ++rb;

        if(rt < 0 && cl < 0 && cr >= colCount && rb >= rowCount){
            globalAnimation = false;
            globalAnimationChanged(globalAnimation);
        }
        else {
            if(rt >= 0) animatedHandles << handleMap[rt*colCount + c];
            if(cl >= 0) animatedHandles << handleMap[r*colCount + cl];
            if(cr < colCount) animatedHandles << handleMap[r*colCount + cr];
            if(rb < rowCount) animatedHandles << handleMap[rb*colCount + c];

            for(int i=0; i<animatedHandles.count(); ++i)
                animatedHandles.at(i)->animate();
        }
    }

    if(!globalAnimation){
        bool winFlag = true;
        foreach(Handle* h, handleMap.values()){
            winFlag = winFlag && h->isHorizontal();
            if(!winFlag) break;
        }
        if(winFlag){
            QTime gameTime = QTime().addMSecs(gameStartTime.msecsTo(QTime::currentTime()));

            QString winText = "Невероятно, молодец! Ваше время: ";
            winText.append(gameTime.toString("HH:mm:ss"));
            winText.append("\n\nВнутри сейфа ");
            winText.append(winList.at(qrand()%winList.count()));
            QMessageBox::information(this, "Поздравляем!", winText);
            configurateUi(GameMenu);
        }
    }
}

void SafePuzzle::onHandleStartRotation(Handle* handle){
    if(!globalAnimation){
        if(!undo){
            QPushButton *undoButton = findChild<QPushButton*>("undoButton");
            if(undoButton) undoButton->setEnabled(true);
            savedSteps << handle;
        } else
            undo = false;

        globalAnimationChanged(true);
    }
}

void SafePuzzle::keyPressEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Escape){
        switch(currentState){
            case Game: {
                int val = QMessageBox::question(this, "Как же так...",
                                                "Выйти в главное меню?", "Нет", "Да");
                if(val) configurateUi(GameMenu);
            } break;

            case GameMenu: {
                int val = QMessageBox::question(this, "Как же так...",
                                                "Выйти из игры?", "Нет", "Да");
                if(val) close();
            } break;

            default:
            case GameStatistic:
            case GameSettings: {
                configurateUi(GameMenu);
            }
        }
    }
    QMainWindow::keyPressEvent(event);
}
