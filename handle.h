#ifndef HANDLE_H
#define HANDLE_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QPaintEvent>
#include <QPainter>
#include <QTimer>
#include <QDebug>
#include <phonon/phonon>

class Handle : public QWidget
{
    Q_OBJECT

signals:
    void handleStartRotation(Handle*);
    void handleRotated(Handle*);

public slots:
    void animate();
    void onGlobalAnimationChanged(bool animation);

public:
    explicit Handle(qreal w, qreal h, Phonon::MediaObject *mediaObject, QWidget *parent = 0);
    ~Handle();
    bool isHorizontal();
    void changeDirection();

protected:
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void updateAngle();

private:
    bool horizontal, animation, globalAnimation;
    Phonon::MediaObject *mediaObject;
    QTimer *timer;
    QPixmap pixmap;
    qreal angle;
};

#endif // HANDLE_H
