#-------------------------------------------------
#
# Project created by QtCreator 2018-03-20T16:36:41
#
#-------------------------------------------------

QT       += core gui phonon

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SafePuzzle
TEMPLATE = app
RC_FILE = res/appInfo.rc


SOURCES += main.cpp\
    handle.cpp \
    safepuzzle.cpp

HEADERS  += \
    handle.h \
    safepuzzle.h

FORMS    += \
    safepuzzle.ui

RESOURCES += \
    res/res.qrc

OTHER_FILES += \
    res/style.qss \
    res/appInfo.rc
