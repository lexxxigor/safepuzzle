#include "handle.h"

Handle::Handle(qreal w, qreal h, Phonon::MediaObject *mediaObject, QWidget *parent) :
    QWidget(parent)
{
    setFixedSize(w, h);
    this->mediaObject = mediaObject;

    pixmap = QPixmap(":/handle").scaledToHeight(qMin(h, w), Qt::SmoothTransformation);

    horizontal = false;

    if(qrand()%2) changeDirection();

    angle = horizontal * 90.0;
    animation = false;
    globalAnimation = false;

    timer = new QTimer(this);
    timer->setInterval(1000/400);
    connect(timer, SIGNAL(timeout()), this, SLOT(animate()));

}

Handle::~Handle(){
}

bool Handle::isHorizontal(){
    return horizontal;
}

void Handle::changeDirection(){
    horizontal = !horizontal;
    updateAngle();
}

void Handle::paintEvent(QPaintEvent *event){
    QPainter painter(this);
    QTransform transform = QTransform()
            .translate(width()*0.5, height()*0.5)
            .rotate(angle)
            .translate(-width()*0.5, -height()*0.5);

    painter.setRenderHints(QPainter::Antialiasing|QPainter::SmoothPixmapTransform);
    painter.setTransform(transform);

    painter.drawPixmap(rect().center()-pixmap.rect().center(), pixmap);
    QWidget::paintEvent(event);
}

void Handle::mouseReleaseEvent(QMouseEvent *event){
    if(event->button() == Qt::LeftButton && !animation && !globalAnimation) animate();
    QWidget::mouseReleaseEvent(event);
}

void Handle::updateAngle(){
    angle = horizontal*90.0;
}

void Handle::animate(){
    if(!animation){
        animation = true;
        handleStartRotation(this);
        timer->start();
        mediaObject->seek(0);
        mediaObject->play();
    }

    if(animation){
        if(!horizontal) angle += 1.0;
        else angle -= 1.0;
    }

    if(angle >= 90.0 || angle <= 0.0){
        timer->stop();
        animation = false;
        horizontal = !horizontal;

        if(angle > 90.0) angle = 90.0;
        else if(angle < 0.0) angle = 0.0;
        handleRotated(this);
    }

    update();
}

void Handle::onGlobalAnimationChanged(bool globalAnimation){
    this->globalAnimation = globalAnimation;
}
