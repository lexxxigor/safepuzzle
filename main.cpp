#include <QApplication>
#include <QTextCodec>
#include <QTime>

#include "safepuzzle.h"

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("cp1251"));
    qsrand(QTime().currentTime().msec());
    QApplication a(argc, argv);
    SafePuzzle w;
    w.show();
    
    return a.exec();
}
