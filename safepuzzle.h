#ifndef SAFEPUZZLE_H
#define SAFEPUZZLE_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QTime>
#include <QPushButton>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPixmap>
#include <QLabel>
#include <QLayout>
#include <QComboBox>
#include <QDebug>

#include "handle.h"

#define click Phonon::MediaSource(":/click")

namespace Ui {
class SafePuzzle;
}

class SafePuzzle : public QMainWindow
{
    Q_OBJECT
    
public:
    enum GameState { GameMenu, Game, GameSettings, GameStatistic };

    explicit SafePuzzle(QWidget *parent = 0);
    ~SafePuzzle();
    
signals:
    void newGame();
    void gameStatistic();
    void globalAnimationChanged(bool);

private:
    Ui::SafePuzzle *ui;
    Phonon::MediaObject *mediaObject;
    QMap<int, Handle*> handleMap;
    QList<Handle*> animatedHandles, savedSteps;

    int rowCount, colCount;
    GameState currentState;
    QStringList winList;
    QTime gameStartTime;
    bool globalAnimation, undo;

private slots:
    void configurateUi(GameState state);
    void onMenuButtonClicked();
    void onUndoButtonClicked();
    void onHandleRotated(Handle *h);
    void onHandleStartRotation(Handle *handle);
    void keyPressEvent(QKeyEvent *event);
};

#endif // SAFEPUZZLE_H
